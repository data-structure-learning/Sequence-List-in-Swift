//
//  Coordinate.swift
//  Sequence-List-in-Swift
//
//  Created by 买明 on 22/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

import Cocoa

class Coordinate: NSObject {
    private var x: Int = 0
    private var y: Int = 0
    
    override var description: String {
        return "(\(x), \(y))"
    }
    
    init(_ x: Int, _ y: Int) {
        self.x = x
        self.y = y
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        return self.x == (object as! Coordinate).x && self.y == (object as! Coordinate).y
    }
}
