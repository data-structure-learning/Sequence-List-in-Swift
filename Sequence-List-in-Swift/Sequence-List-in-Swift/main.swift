//
//  main.swift
//  Sequence-List-in-Swift
//
//  Created by 买明 on 22/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

import Foundation

func testIntSqList() {
    let sqList = SequenceList<Int>(size: 5)
    
    var res = sqList.insertElem(1, at: 0)
    res = sqList.insertElem(2, at: 1)
    res = sqList.insertElem(3, at: 2)
    res = sqList.insertElem(4, at: 3)
    res = sqList.insertElem(10, at: 0)
    
    sqList.listTraverse()
    
    res = sqList.insertElem(6, at: 5)
    res = sqList.insertElem(-1, at: -1)
    
    sqList.listTraverse()

    var e = 0
    e = sqList.deleteElem(at: 3).1!
    print("e: \(e)")
    
    sqList.listTraverse()
    
    res = sqList.insertElem(10, at: 3)
    
    if res {
        sqList.listTraverse()
    }
    
    sqList.clearList()
    
    sqList.listTraverse()
}

func testObjectSqList() {
    let sqList = SequenceList<Coordinate>(size: 5)
    
    var res = sqList.insertElem(Coordinate(1, 1), at: 0)
    res = sqList.insertElem(Coordinate(2, 2), at: 1)
    res = sqList.insertElem(Coordinate(3, 3), at: 2)
    res = sqList.insertElem(Coordinate(4, 4), at: 3)
    res = sqList.insertElem(Coordinate(10, 10), at: 0)
    
    sqList.listTraverse()
    
    res = sqList.insertElem(Coordinate(6, 6), at: 5)
    res = sqList.insertElem(Coordinate(-1, -1), at: -1)
    
    sqList.listTraverse()
    
    var e = Coordinate(0, 0)
    e = sqList.deleteElem(at: 3).1!
    print("e: \(e)")
    
    sqList.listTraverse()
    
    res = sqList.insertElem(Coordinate(10, 10), at: 3)
    
    if res {
        sqList.listTraverse()
    }
    
    sqList.clearList()
    
    sqList.listTraverse()
}

print("testIntSqList")
testIntSqList()

print("testObjectSqList")
testObjectSqList()
