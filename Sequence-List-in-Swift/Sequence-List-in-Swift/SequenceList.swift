//
//  SequenceList.swift
//  Sequence-List-in-Swift
//
//  Created by 买明 on 22/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

import Cocoa

class SequenceList<T: Equatable>: NSObject {
    private var m_pList: [T] = []
    private var m_iSize: Int = 0
    private var m_iLen: Int = 0
    
    init(size: Int) {
        m_iSize = size
        m_iLen = 0
    }
    
    func clearList() {
        m_iLen = 0
    }
    
    func isEmpty() -> Bool {
        return m_iLen == 0
    }
    
    func isFull() -> Bool {
        return m_iLen == m_iSize
    }
    
    func getAt(_ index: Int) -> T? {
        if index < 0 || index >= m_iLen {
            return nil
        }
        
        return m_pList[index]
    }
    
    func locate(elem: T) -> Int {
        for i in stride(from: 0, to: m_iLen, by: 1) {
            if m_pList[i] == elem {
                return i
            }
        }
        
        return -1
    }
    
    func getPriorBy(currElem: T) -> T? {
        let t = locate(elem: currElem)
        
        if t == -1 {
            return nil
        }
        
        return m_pList[t - 1]
    }
    
    func getNextBy(currElem: T) -> T? {
        let t = locate(elem: currElem)
        
        if t == m_iLen - 1 {
            return nil
        }
        
        return m_pList[t + 1]
    }
    
    func insertElem(_ elem: T, at index: Int) -> Bool {
        if index < 0 || index > m_iLen || isFull() {
            return false
        }
        
        if index < m_iLen {
            m_pList.append(m_pList[m_iLen - 1])
        }
        for i in stride(from: m_iLen - 2, through: index, by: -1) {
            m_pList[i + 1] = m_pList[i]
        }

        if m_pList.count <= index {
            m_pList.append(elem)
        } else {
            m_pList[index] = elem
        }
        
        m_iLen += 1
        return true
    }
    
    func deleteElem(at index: Int) -> (Bool, T?) {
        if index < 0 || index >= m_iLen || isEmpty() {
            return (false, nil)
        }
        let elem = m_pList[index]
        for i in stride(from: index, to: m_iLen - 1, by: 1) {
            m_pList[i] = m_pList[i + 1]
        }
        m_iLen -= 1;
        return (true, elem)
    }
    
    func listTraverse() {
        for i in stride(from: 0, to: m_iLen, by: 1) {
            print(m_pList[i], separator: "", terminator: " ")
        }
        print("")
    }
}
